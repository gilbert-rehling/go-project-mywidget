package main

// imports
import (
	"encoding/json"
	"net/http"
	"log"
	"regexp"
)

// container for the widgets
type Widget struct {
	ID string
	Name string
}

// return a single widget item
func ReturnItem (w http.ResponseWriter, req *http.Request) {

	var validId = regexp.MustCompile("^([a-zA-Z]+)$")

	// get the id parameter
	id := req.URL.Query().Get("id")

	if id != "" {
		// validate the supplied value
		m := validId.FindStringSubmatch(id)
		if m == nil {
			// return an error if request id is invalid
			http.NotFound(w, req)
			return
		}
		response, err := getJsonResponseItem(id);
		if err != nil {
			// error generating JSON response
			http.NotFound(w, req)
			return
		}

		// convert the response to string to ensure its not NULL
		myString := string(response[:])

		if myString != "null" {
			// set content type to return JSON
			w.Header().Set("Content-Type", "application/json")
			w.Write(response)

		} else {
			// if widget is not found
			http.NotFound(w, req)
			return
		}

	} else {
		// if the id query string is not found
		http.NotFound(w, req)
		return
	}
}

// returns the full set of widgets
func ReturnItems(w http.ResponseWriter, req *http.Request) {

	// data type to store the complete widgets set
	type Items [] Widget

	// populate the dataset
	var items = Items {
		Widget {
			"loc",
			"GeoLocation",
		},
		Widget {
			"test",
			"Testing",
		},
		Widget{
			"serv",
			"Service",
		},
		Widget {
			"help",
			"Helping",
		},
		Widget {
			"hello",
			"HelloWorld",
		},
		Widget {
			"cal",
			"Calendar",
		},
	}

	// convert dataset to a JSON response
	response, err := json.Marshal(items)
	if err != nil {
		log.Fatal("Cannot encode to JSON ", err)
		// error generating JSON response
		http.NotFound(w, req)
		return
	}

	// output the JSON data
	w.Header().Set("Content-Type", "application/json")
	w.Write(response)
}

// main function
func main() {

	// configuration - fixed IP or leave blank
	var server = "192.168.1.140"
	// configuration - service port !! required !!
	var port   = "8080"

	// handles the single widget request
	http.HandleFunc("/widget", ReturnItem)

	// handles the 'all widgets' request
	http.HandleFunc("/widgets/all", ReturnItems)

	// set to listen on external facing IP address on DEV box
	log.Fatal(http.ListenAndServe(server + ":" + port, nil))
}

// return a JSON response with a single row
func getJsonResponseItem (id string)([]byte, error) {

	// widgets stored as a multi dimensional array
	var data [6][2]string
	data[0][0] = "loc"
	data[0][1] = "GeoLocation"
	data[1][0] = "test"
	data[1][1] = "Testing"
	data[2][0] = "serv"
	data[2][1] = "Service"
	data[3][0] = "help"
	data[3][1] = "Helping"
	data[4][0] = "hello"
	data[4][1] = "HelloWorld"
	data[5][0] = "cal"
	data[5][1] = "Calendar"

	var i, j int
	// search for a widget ket matching the id provided
	for i = 0; i < 6; i++ {
		for j = 0; j < 2; j++ {
			if id == data[i][j] {
				m := Widget{ data[i][j], data[i][j+1] }
				return json.Marshal(m)
				break
			}
		}
	}
	// this will return a null byte which will need to be converted to string to test is content
	return json.Marshal(nil)
}
