#Go Project - MyWidget

My first GoLang project.

Using the GoLang http service to bind a web server to the configured service port.

Requests to ~:<port>/widget?id=<widget key> will return a single JSON data row.

Requests to ~:<port>/widgets/all will return the full set of widgets as JSON data.

Note: I have opted to use the pluralised version of the single item URI to fetch the full data list.

The code provides two examples of storing and generating the widgets data and sending the response.

For the 'single-widget case' data is stored within a multi dimensional array.

For the 'full-set case' data is contained with a Json Serializable object, encoded as JSON then returned with the http response.

Added a PHP service with this repo which will be used to query the GoLang service.

PHP service includes service class file, configuration file and sample front-end script.

Demo page: http://go.gilbert-rehling.com

GoLang service: http://go.gilbert-rehling.com:8080/widgets/all

Should the GoLang service be unavailable: http://go.gilbert-rehling.com/loadgo.php
