<?php

/**
 * Demonstration usage of the Widgets service
 *
 * This implementations handles all data variations and error responses
 */

// include the service
include_once('./services/myWidgetService.php');

// create a service instance
$myWidgets = new MyWidgets();

// check for a single widget request
if ( isset($_REQUEST['key']) && !empty($_REQUEST['key']) && $_REQUEST['key'] != 'all' ) {

    // fetch the filtered request string
    $widgets = $myWidgets->getWidget( filter_var ( $_REQUEST['key'], FILTER_SANITIZE_STRING) );

} else {

    // get all widgets
    $widgets = $myWidgets->getAllWidgets( );

}

?>
<!DOCTYPE html >
<html data-ng-app="main">
<head>
    <meta charset="utf-8" />
    <title>My Widgets</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body data-ng-controller="PublicCtrl" class="muse-body">
<!-- header section -->
<header class="header-content text-center" id="header-content" data-ng-show="!hasConfirmed()">
    <div class="header-inner">
        <div class="container-fluid">
            <div><h1>My Widgets</h1></div>
            <p>&nbsp;</p>
        </div>
    </div>
</header>
<section class="text-center">
<?php  if (isset($widgets) && (is_array($widgets) || is_object($widgets))) : ?>

    <strong>Displaying <?php echo count($widgets); ?> <?php echo count($widgets) >= 2 ? 'widgets' : 'widget'; ?>:</strong>
    <p>&nbsp;</p>

    <!-- we require 2 indexes to handle single and multiple display instances -->
    <?php $i = 0; foreach ($widgets as $outer => $object) : $index = 0; ?>

        <?php if (is_object($object)) : ?>
        <p>
            <?php foreach ($object as $inner => $value) : ?>

                <?php if ($index == 0) : ?>

                    <strong>Key:</strong> <?php echo $value; $index++; ?>,

                <?php else: ?>

                    <strong>Value:</strong> <?php echo $value; ?>

                <?php endif; ?>

            <?php endforeach; ?>
        </p>
        <?php else: ?>

            <?php if ($i == 0) : ?>

                <p>
                <strong>Request:</strong> <?php echo $object; $i++; ?>,

            <?php else: ?>

                <strong>Response:</strong> <?php echo $object; ?>
                </p>

            <?php endif; ?>

        <?php endif; ?>

    <?php endforeach; ?>

<?php else : ?>

    <?php if (isset($widgets) && strlen($widgets)) : ?>

        <p><strong>Error:</strong> <?php echo $widgets; ?></p>

    <?php else : ?>

        <p>There are no widgets to display...</p>

    <?php endif; ?>

<?php endif; ?>
</section>
<footer>
    <div class="footer-inner text-center">
        <div class="container-fluid">
            <p>&nbsp;</p>
            <p>Copyright (c) 2017 Gilbert Rehling</p>
        </div>
    </div>
</footer>