<?php
/**
 * Widget Service by Gilbert Rehling
 *
 * @author Gilbert Rehling
 * @email  gilbert@gilbert-rehling.com
 * @url    http://go.gilbert-rehling.com
 *
 * Usage:
 * Include this file into the script were you would like to run this service
 * include_once('services/myWidgetService.php');
 *
 * Initialise the class:
 * $myWidgets = new MyWidgets();
 *
 * To fetch a single item: (requires a variable containing the requested widget key)
 * $widget = $myWidgets->getWidget( $key );
 *
 * To fetch all widgets:
 * $widgets = $myWidgets->getAllWidgets();
 *
 */

/**
 * Include the configuration - ensure the 'serviceConfiguration.php' file was included with this service package
 * This will throw a PHP error of the file is not found
 */
require_once('serviceConfiguration.php');

/**
 * Testing
 * Uncomment a test case below to force an error response from CURL
 */
//ServiceConfiguration::$protocol = 'htpp';
//ServiceConfiguration::$host = 'gobbeldy.com';
//ServiceConfiguration::$port = '808080';

/**
 * Class MyWidgets
 */
class MyWidgets {

    /**
     * Method handles the requests sent using CURL to the GoLang service which is expected to return a JSON response
     *
     * @param bool $key (key is passed from the local public method)
     * @return mixed|string
     */

    private function handleRequest( $key = false )
    {

        // determine the correct protocol to use
        $proto = ((isset( ServiceConfiguration::$protocol ) && strlen( ServiceConfiguration::$protocol )) ? ServiceConfiguration::$protocol : ((isset($_SERVER['HTTPS']) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == true)) ? 'https' : 'http'));

        // get and set the correct host
        $host = isset( ServiceConfiguration::$host ) && strlen( ServiceConfiguration::$host ) ?  ServiceConfiguration::$host : $_SERVER['SERVER_NAME'];

        // get and set the PORT
        $port = isset( ServiceConfiguration::$port ) && strlen( ServiceConfiguration::$port ) ?  ':' . ServiceConfiguration::$port : '';

        if ($key) {
            // single item requested
            $url = $proto . '://' . $host . $port . ServiceConfiguration::$itemUrl . "?" . ServiceConfiguration::$itemParam . '=' . $key;

        } else {
            // fetch all items
            $url = $proto . '://' . $host . $port .  ServiceConfiguration::$listUrl;
        }

        /** use CURL the fetch the JSON data */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_FILETIME, 1);

        // Todo: get SSL without CERT check (simple method for now)
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $response = curl_exec ($ch);

        if (curl_errno($ch)) {
            // get the error response
            $error = curl_error($ch);
            $response = sprintf("Unable to continue due to CURL error: %s", $error);
        }
        curl_close($ch);

        // handle a 404 error - may occur if invalid widget requested
        if (strpos($response, '404') !== false) {
            $response = "Requested widget not found.";
        }

        // errors wont contain a curly brace
        if (strpos($response, "{") !== false) {
            /**
             * Return the decoded JSON data
             * The returned data will still contain objects - another method to convert to an array would simplify the output iterations
             */
            return json_decode($response);
        }

        // return an error response string
        return $response;
    }

    /**
     * Method handles requests for a single widget item
     *
     * @param $key (key passed to service from calling page/script)
     * @return mixed|string
     */
    public function getWidget( $key )
    {
        return $this->handleRequest( $key );
    }

    /**
     * Method handles requests for the full lists of widgets
     *
     * @return mixed|string
     */
    public function getAllWidgets()
    {
        return $this->handleRequest( );
    }
}
