<?php
/**
 * Created by PhpStorm.
 * User: gilbertr
 * Date: 6/08/2017
 * Time: 8:15 PM
 *
 * Widget service configuration
 */

class ServiceConfiguration {

    /**
     * service URI - leave blank to make it relative
     * @var string
     */
    static $host = '192.168.1.140';

    /**
     * service PORT - leave blank to use the standard HTTP port
     * @var string
     */
    static $port = '8080';

    /**
     * service PROTOCOL - leave blank to use current
     * @var string
     */
    static $protocol = 'http';

    /**
     * single widget URI
     * @var string
     */
    static $itemUrl = '/widget';

    /**
     * single widget request query parameter
     * @var string
     */
    static $itemParam = 'id';

    /**
     * widgets list URI
     * @var string
     */
    static $listUrl = '/widgets/all';
}
